/*
 * This file is provided under a dual BSD/GPLv2 license.  When using or
 * redistributing this file, you may do so under either license.
 *
 * GPL LICENSE SUMMARY
 *
 * Copyright(c) 2008 - 2011 Intel Corporation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 * The full GNU General Public License is included in this distribution
 * in the file called LICENSE.GPL.
 *
 * BSD LICENSE
 *
 * Copyright(c) 2008 - 2011 Intel Corporation. All rights reserved.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   * Neither the name of Intel Corporation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <asm/types.h>
#include <strings.h>
#include <stdint.h>

#include "create_fw.h"

int write_blob(struct isci_orom *isci_orom)
{
	FILE *fd;
	int err;
	size_t count;

	fd = fopen(blob_name, "w+");
	if (!fd) {
		perror("Open file for write failed");
		fclose(fd);
		return -EIO;
	}

	count = fwrite(isci_orom, sizeof(struct isci_orom), 1, fd);
	if (count != 1) {
		perror("Write data failed");
		fclose(fd);
		return -EIO;
	}

	fclose(fd);

	return 0;
}

void set_binary_values(struct isci_orom *isci_orom)
{
	int c, phy_idx, port_idx;

	/* setting OROM signature */
	strncpy(isci_orom->hdr.signature, sig, strlen(sig));
	isci_orom->hdr.version = version;
	isci_orom->hdr.total_block_length = sizeof(struct isci_orom);
	isci_orom->hdr.hdr_length = sizeof(struct sci_bios_oem_param_block_hdr);
	isci_orom->hdr.num_elements = num_elements;

	for (c = 0; c < 2; c++) {
		struct sci_oem_params *ctrl = &isci_orom->ctrl[c];
		__u8 cable_selection_mask = 0;

		ctrl->controller.mode_type = mode_type;
		ctrl->controller.max_concurr_spin_up = max_num_concurrent_dev_spin_up;
		ctrl->controller.do_enable_ssc = enable_ssc;

		for (port_idx = 0; port_idx < SCI_MAX_PORTS; port_idx++)
			ctrl->ports[port_idx].phy_mask = phy_mask[c][port_idx];

		for (phy_idx = 0; phy_idx < SCI_MAX_PHYS; phy_idx++) {
			struct sci_phy_oem_params *phy = &ctrl->phys[phy_idx];
			__u8 cable_phy = cable_selection[c][phy_idx];

			phy->sas_address.high = sas_addr[c][phy_idx] >> 32;
			phy->sas_address.low = sas_addr[c][phy_idx];

			phy->afe_tx_amp_control0 = afe_tx_amp_control0;
			phy->afe_tx_amp_control1 = afe_tx_amp_control1;
			phy->afe_tx_amp_control2 = afe_tx_amp_control2;
			phy->afe_tx_amp_control3 = afe_tx_amp_control3;

			cable_selection_mask |= (cable_phy & 1) << phy_idx;
			cable_selection_mask |= (cable_phy & 2) << (phy_idx + 3);
		}
		ctrl->controller.cable_selection_mask = cable_selection_mask;
	}
}

int main(void)
{
	int err;
	struct isci_orom *isci_orom;

	isci_orom = malloc(sizeof(struct isci_orom));
	memset(isci_orom, 0, sizeof(struct isci_orom));

	set_binary_values(isci_orom);

	err = write_blob(isci_orom);
	if (err < 0) {
		free(isci_orom);
		return err;
	}

	free(isci_orom);
	return 0;
}
