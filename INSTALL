Briefly, running "make" without specifying any targets will build
everything. You may not necessarily want everything though and can
also specify targets to make. For example:

    make av7110

Please see the included Makefile for a list of all available targets.

Once the desired things are built, "make install" will put them into
the appropriate place, which is /lib/firmware by default although you
can override this with something like:

    make prefix=/desired/path install

In order to build everything you will need the following on the host
system:

    * A C/C++ compiler, like GCC
    * Cmake
    * GNU Bison/YACC
    * GNU Flex
    * GNU Gperf
    * GNU Make
    * GNU Wget
    * GNU C cross-compiler for ARM:
        - arm-linux-gnueabi-gcc
        - arm-linux-gnueabi-ld
        - arm-linux-gnueabi-objcopy
        - arm-none-eabi-gcc
        - arm-none-eabi-objcopy
        - arm-none-eabi-as

On GNU/Linux distros that use apt you can install these with:

    apt install binutils-arm-linux-gnueabi binutils-arm-none-eabi bison \
    cmake flex g++ gcc gcc-arm-linux-gnueabi gcc-arm-none-eabi gperf make wget

CARL9170 Firmware Configuration
When building the carl9170 firmware you will be prompted with
configuration questions.

Licensing
---------

You can redistribute and/or modify this file under the terms of the
GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any
later version.