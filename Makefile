# Copyright (C) 2017, 2018 Jason Self <j@jxself.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

shell=/bin/sh
prefix=/lib/firmware
install_program=install

.PHONY:	all test clean install a56 as31 aica ath9k_htc_toolchain ath9k_htc_firmware av7110 b43-tools carl9170fw-toolchain carl9170fw cis-tools cis dsp56k ihex2fw isci keyspan_pda openfwwf usbdux

all: aica ath9k_htc av7110 carl9170fw cis dsp56k isci keyspan_pda openfwwf usbdux

a56:
	cd a56 && $(MAKE)

as31:
	cd as31 && ./configure && $(MAKE)

aica:
	cd aica/arm && $(MAKE)

ath9k_htc_toolchain:
	cd ath9k_htc && $(MAKE) toolchain

ath9k_htc: ath9k_htc_toolchain
	cd ath9k_htc && $(MAKE) -C target_firmware

av7110:
	cd av7110 && $(MAKE)

b43-tools:
	cd b43-tools/assembler && $(MAKE)

carl9170fw-toolchain:
	cd carl9170fw && $(MAKE) -C toolchain

carl9170fw: carl9170fw-toolchain
	cd carl9170fw && ./autogen.sh

cis: cis-tools
	cd cis && $(MAKE)

cis-tools:
	cd cis-tools && $(MAKE)

dsp56k: a56
	cd dsp56k && $(MAKE)

ihex2fw:
	cd ihex2fw && $(MAKE)

isci:
	cd isci && $(MAKE)

keyspan_pda: ihex2fw as31
	cd keyspan_pda && $(MAKE)

openfwwf: b43-tools
	cd openfwwf && $(MAKE)

usbdux: as31
	cd usbdux && $(MAKE) -f Makefile_dux

test:
	@echo This function is not implemented.

clean:
	cd aica/arm && $(MAKE) clean
	cd a56 && $(MAKE) clean
	if [ -a as31/Makefile ]; then cd as31 && $(MAKE) clean; fi;
	cd ath9k_htc && $(MAKE) toolchain-clean
	cd ath9k_htc && $(MAKE) -C target_firmware clean
	cd av7110 && $(MAKE) clean
	cd carl9170fw/toolchain && $(MAKE) clean
	if [ -a carl9170fw/Makefile ]; then cd carl9170fw && $(MAKE) clean; fi;
	cd dsp56k && $(MAKE) clean
	cd ihex2fw && $(MAKE) clean
	cd cis-tools && $(MAKE) clean
	cd cis && $(MAKE) clean
	cd isci && $(MAKE) clean
	cd keyspan_pda && $(MAKE) clean
	cd openfwwf && $(MAKE) clean
	cd usbdux && $(MAKE) -f Makefile_dux clean

install:
	if [ -a aica/arm/aica_firmware.bin ]; then $(install_program) -D aica/arm/aica_firmware.bin $(prefix)/aica_firmware.bin; fi;
	if [ -a ath9k_htc/target_firmware/htc_9271.fw ]; then $(install_program) -D ath9k_htc/target_firmware/htc_9271.fw $(prefix)/ath9k_htc/htc_9271-1.4.0.fw && \
		ln -s ath9k_htc/htc_9271-1.4.0.fw $(prefix)/htc_9271.fw; fi;
	if [ -a ath9k_htc/target_firmware/htc_7010.fw ]; then $(install_program) -D ath9k_htc/target_firmware/htc_7010.fw $(prefix)/ath9k_htc/htc_7010-1.4.0.fw && \
		ln -s ath9k_htc/htc_7010-1.4.0.fw $(prefix)/htc_7010.fw; fi;
	if [ -a av7110/bootcode.bin ]; then $(install_program) -D av7110/bootcode.bin $(prefix)/av7110/bootcode.bin; fi;
	if [ -a cis/3CCFEM556.cis ]; then $(install_program) -D cis/3CCFEM556.cis $(prefix)/cis/3CCFEM556.cis; fi;
	if [ -a cis/3CXEM556.cis ]; then $(install_program) -D cis/3CXEM556.cis $(prefix)/cis/3CXEM556.cis; fi;
	if [ -a cis/COMpad2.cis ]; then $(install_program) -D cis/COMpad2.cis $(prefix)/cis/COMpad2.cis; fi;
	if [ -a cis/COMpad4.cis ]; then $(install_program) -D cis/COMpad4.cis $(prefix)/cis/COMpad4.cis; fi;
	if [ -a cis/DP83903.cis ]; then $(install_program) -D cis/DP83903.cis $(prefix)/cis/DP83903.cis; fi;
	if [ -a cis/LA-PCM.cis ]; then $(install_program) -D cis/LA-PCM.cis $(prefix)/cis/LA-PCM.cis; fi;
	if [ -a cis/MT5634ZLX.cis ]; then $(install_program) -D cis/MT5634ZLX.cis $(prefix)/cis/MT5634ZLX.cis; fi;
	if [ -a cis/NE2K.cis ]; then $(install_program) -D cis/NE2K.cis $(prefix)/cis/NE2K.cis; fi;
	if [ -a cis/PCMLM28.cis ]; then $(install_program) -D cis/PCMLM28.cis $(prefix)/cis/PCMLM28.cis; fi;
	if [ -a cis/PE520.cis ]; then $(install_program) -D cis/PE520.cis $(prefix)/cis/PE520.cis; fi;
	if [ -a cis/PE-200.cis ]; then $(install_program) -D cis/PE-200.cis $(prefix)/cis/PE-200.cis; fi;
	if [ -a cis/RS-COM-2P.cis ]; then $(install_program) -D cis/RS-COM-2P.cis $(prefix)/cis/RS-COM-2P.cis; fi;
	if [ -a cis/SW_7xx_SER.cis ]; then $(install_program) -D cis/SW_7xx_SER.cis $(prefix)/cis/SW_7xx_SER.cis; fi;
	if [ -a cis/SW_8xx_SER.cis ]; then $(install_program) -D cis/SW_8xx_SER.cis $(prefix)/cis/SW_8xx_SER.cis; fi;
	if [ -a cis/SW_555_SER.cis ]; then $(install_program) -D cis/SW_555_SER.cis $(prefix)/cis/SW_555_SER.cis; fi;
	if [ -a cis/tamarack.cis ]; then $(install_program) -D cis/tamarack.cis $(prefix)/cis/tamarack.cis; fi;
	if [ -a carl9170fw/carlfw/carl9170.fw ]; then cd carl9170fw && ./autogen.sh install && 	$(install_program) -D carl9170-1.fw $(prefix)/carl9170-1.fw; fi;
	if [ -a dsp56k/bootstrap.bin ]; then $(install_program) -D dsp56k/bootstrap.bin $(prefix)/dsp56k/bootstrap.bin; fi;
	if [ -a isci/isci_firmware.bin ]; then $(install_program) -D isci/isci_firmware.bin $(prefix)/isci/isci_firmware.bin; fi
	if [ -a keyspan_pda/keyspan_pda.fw ]; then $(install_program) -D isci/isci_firmware.bin $(prefix)/keyspan_pda/keyspan_pda.fw; fi
	if [ -a keyspan_pda/keyspan_pda.fw ]; then $(install_program) -D isci/isci_firmware.bin $(prefix)/keyspan_pda/xircom_pgs.fw; fi
	if [ -a openfwwf/ucode5.fw ]; then $(install_program) -D openfwwf/ucode5.fw $(prefix)/b43-open/ucode5.fw; fi
	if [ -a openfwwf/b0g0bsinitvals5.fw ]; then $(install_program) -D openfwwf/b0g0bsinitvals5.fw $(prefix)/b43-open/b0g0bsinitvals5.fw; fi
	if [ -a openfwwf/b0g0initvals5.fw ]; then $(install_program) -D openfwwf/b0g0initvals5.fw $(prefix)/b43-open/b0g0initvals5.fw; fi
	if [ -a usbdux/usbduxfast_firmware.bin ]; then $(install_program) -D usbdux/usbduxfast_firmware.bin $(prefix)/usbduxfast_firmware.bin; fi
	if [ -a usbdux/usbdux_firmware.bin ]; then $(install_program) -D usbdux/usbdux_firmware.bin $(prefix)/usbdux_firmware.bin; fi
	if [ -a usbdux/usbduxsigma_firmware.bin ]; then $(install_program) -D usbdux/usbduxsigma_firmware.bin $(prefix)/usbduxsigma_firmware.bin; fi
