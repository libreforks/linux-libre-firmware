/*******************************************************
 *
 *  a56 - a DSP56001 assembler
 *
 *  Written by Quinn C. Jensen
 *  July 1990
 *
 *******************************************************\

/*
 * Copyright (C) 2012 Thorsten Alteholz <debian@alteholz.de>
 *
 * This file is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * This file incorporates work covered by the following copyright and
 * permission notice:
 *
 * Copyright (C) 1990-1994 Quinn C. Jensen
 *
 * Permission to use, copy, modify, distribute, and sell this software
 * and its documentation for any purpose is hereby granted without fee,
 * provided that the above copyright notice appear in all copies and
 * that both that copyright notice and this permission notice appear
 * in supporting documentation.  The author makes no representations
 * about the suitability of this software for any purpose.  It is
 * provided "as is" without express or implied warranty.
 *
 */
static char *Copyright = "Copyright (C) 1990-1994 Quinn C. Jensen";

#include <stdio.h>

/*
 *  This small program converts the a56.out file from the assembler
 *  into a file suitable for loading into 56001 memory via the
 *  SLOADER.ASM serial loader program provided on the Motorola
 *  Dr. Bubb BBS.
 *
 */

#define MAX 256

main(argc,argv)
int argc;
char *argv[];
{
	char buf[MAX];
	int curaddr = 0;
	int line = 0;
	int curseg = '\0';
	int startaddr = -1;

	while(gets(buf)) {
		char seg;
		int addr, data;
		line++;
		if(sscanf(buf, "%c%x%x", &seg, &addr, &data) == 3) {
			if(seg == 'I' || seg == 'F') break;
			if(seg != curseg || curaddr != addr) {
				printf("\n_DATA %c %04X\n", curseg = seg, curaddr = addr);
			}   
			if(startaddr == -1 && seg == 'P')
				startaddr = addr;
			printf("%06X ", data & 0xFFFFFF);
			curaddr++;
		}
	}
	printf("\n_END %04X\n", startaddr);
}
